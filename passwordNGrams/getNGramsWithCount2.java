import java.io.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.HashMap;

public class getNGramsWithCount {
    public class Pair{
      char L;
      double R;
    }

    public static void main(String [] args) throws FileNotFoundException {

      // The name of the file to open.
      //String fileName = "rockyou.txt";
      ArrayList<String> lst = new ArrayList<String>();
//      lst.add("rockyou.txt");
      lst.add("myspace.txt");

      BufferedWriter output = null;
      // This will reference one line at a time
      String line = null;

      File file = new File("sample2.txt");
//      output.write(text);

        FileOutputStream file2 = new FileOutputStream("chain.ser");

      HashMap<String, HashMap<Character, Integer>> map = new HashMap<String, HashMap<Character,Integer>>();

      try {
        for(String fileName: lst){
          // FileReader reads text files in the default encoding.
        FileReader fileReader =
        new FileReader(fileName);
        //output = new BufferedWriter(new FileWriter(file));
        // Always wrap FileReader in BufferedReader.
        BufferedReader bufferedReader =
        new BufferedReader(fileReader);
        while((line = bufferedReader.readLine()) != null) {
          //Here i should add a regex if it satisfies our language
          //Lets just look for passwords in our thing
          //Also add an end/start marker?
          // Check to see is string contains any non-ascii characters.
          boolean ascii = true;
          for(char c : line.toCharArray()) {
            if ((int) c < 33 || (int) c > 126) {
              ascii = false;
              break;
            }
          }

          if (!ascii) {
            continue;
          }

          line = (char)9664 + line + (char)9654;
          //here we have the password we want
          for(int i =0; i < line.length()-4; i++){

            String fourGram = line.substring(i,i+4);
            char next = line.charAt(i+4);
            if(map.containsKey(fourGram)){
              if(map.get(fourGram).containsKey(next)){
                int ct = map.get(fourGram).get(next) + 1;
                map.get(fourGram).put(next, ct);
              }
              else{
                map.get(fourGram).put(next,1);
              }
            }
            else{//map does not contain the n-gram
              HashMap<Character,Integer> tmp = new HashMap<Character,Integer>();
              tmp.put(next,1);
              map.put(fourGram, tmp);
            }
          }
        }

        /*
        // Always close files.
        */
        //now we write to file
        HashMap<String, LinkedHashMap<String, Double>> out = new HashMap<String, LinkedHashMap<String, Double>>();
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry pair = (Map.Entry)it.next();
          //System.out.println(pair.getKey() + " = " + pair.getValue());
          HashMap<Character, Integer> currMap = (HashMap<Character, Integer>)pair.getValue();
          Iterator counter = currMap.entrySet().iterator();
          double n=0;
          while(counter.hasNext()){
            Map.Entry currPair = (Map.Entry)counter.next();
            Integer tmp = (Integer)currPair.getValue();
            n+= tmp.doubleValue();
//            counter.remove();
          }

          Iterator it2 = currMap.entrySet().iterator();
          //output.write((String)pair.getKey() + '\t');

            LinkedHashMap<String, Double> prob2 = new LinkedHashMap<String, Double>();
          while(it2.hasNext()){
            Map.Entry currPair = (Map.Entry)it2.next();
            Integer ct = (Integer)currPair.getValue();
            Double prob = (999.0/1000.0)*(ct.doubleValue())/n;
              prob2.put(String.valueOf((char) currPair.getKey()), prob);
            //output.write("[" + currPair.getKey() + ", " + prob + "] " + '\t');
            it2.remove();
          }
            if (prob2.keySet() == null) {
              prob2.put("other", 1.0);
            }
            else {
              prob2.put("other", 1.0 / 1000.0);
            }
          out.put((String) pair.getKey(), prob2);

          //output.write('\n');
          it.remove(); // avoids a ConcurrentModificationException
        }

            ObjectOutputStream write = new ObjectOutputStream(file2);
            write.writeObject(out);
            write.close();
        bufferedReader.close();
        //now we write to file
      }
    }
    catch(FileNotFoundException ex) {
      System.out.println( "Unable to open file '" );
    }
    catch(IOException ex) {
      System.out.println( "Error reading file '" );
            // Or we could just do this:
            // ex.printStackTrace();
    }
  }
}
