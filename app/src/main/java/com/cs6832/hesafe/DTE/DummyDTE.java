package com.cs6832.hesafe.DTE;

/*dummy DTE used to test functionality Encryption*/

public class DummyDTE extends DTE{
	public DummyDTE(){
		System.out.println("You made a dummyDTE!");
	}
	public String encode(String in){
		return in;
	}
	public String decode(String out){
		return out;
	}
}
