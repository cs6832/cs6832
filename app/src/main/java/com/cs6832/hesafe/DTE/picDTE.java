package com.cs6832.hesafe.DTE;
import com.cs6832.hesafe.Utils; //is this safe?
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class PicDTE extends DTE{
	private SecureRandom rand;
	public PicDTE() {
		try {
			rand = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Error occured in DTE constructor");
			e.printStackTrace();
		}
	}
	/*precondition: m is in {0,1}^8 assume that m will always be an integer
	* that is the message is a number between 0 and 2^7 -1 
	*postcondition: c is in {0,1}^256
	*/
	public String encode(String message){
		System.out.println("encode: " + message);
		int i = Integer.parseInt(message); //this message should be no more than 8bits long
		byte bytePicID = (byte)i;
		//assert(byteArr.length == 1);
		byte[] tmp = new byte[32];
		rand.nextBytes(tmp);
		tmp[0] = bytePicID;
		return Utils.getBinaryStringfromBytes(tmp);
	}

	/*precondition: c is in {0,1}^256
	* postcondition: m is in {0,1}^8
	*/
	public String decode(String message) {
		try {
			byte[] byteArr = Utils.getBytesFromBinaryString(message);
			byte[] tmp = new byte[1];
			tmp[0] = byteArr[0];
			System.out.println("tmp: " + Utils.getBinaryStringfromBytes(tmp));
			System.out.println("tmp2: " + Utils.getBinaryStringfromBytes(new String(tmp, "UTF-8").getBytes("UTF-8")));
			return Utils.getBinaryStringfromBytes(tmp);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
