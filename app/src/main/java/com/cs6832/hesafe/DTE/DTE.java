/*abstract wrapper class for DTE encoders*/
package com.cs6832.hesafe.DTE;

public abstract class DTE{
	public abstract String encode(String password) throws Exception;
	public abstract String decode(String seed) throws Exception;
}

