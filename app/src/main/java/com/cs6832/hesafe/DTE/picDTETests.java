package com.cs6832.hesafe.DTE;
import com.cs6832.hesafe.Utils;


public class picDTETests{
	public static void main(String[] args){
		PicDTE dte = new PicDTE();
		byte test1 = (byte)0b01111111;
		byte test2 = (byte)0b00101010;
		byte test3 = (byte)0b01110000;
		byte[] t1 = new byte[1];
		byte[] t2 = new byte[1];
		byte[] t3 = new byte[1];
		t1[0] = test1;
        Utils.printBytes(t1);
        System.out.println(" ");
        t2[0] = test2;
		t2[0] = test3;
		String t1s = Utils.getString(t1);
		String t2s = Utils.getString(t2);
		String t3s = Utils.getString(t3);
        //System.out.println("Before:" + t1s + ", " + t2s + ", " + t3s);
        String after1 = dte.encode(t1s);
        Utils.printBytes(Utils.getBytes(after1));

        String after2 = dte.decode(dte.encode(t2s));
        String after3 = dte.decode(dte.encode(t3s));
        System.out.println("After: " + after1 + ", " + after2 + ", " + after3);
		/*assert(dte.decode(dte.encode(t1s)).equals(t1s));
		assert(dte.decode(dte.encode(t2s)).equals(t2s));
		assert(dte.decode(dte.encode(t3s)).equals(t3s));
*/
        Utils.printBytes(Utils.getBytes(after1));
	}
}
