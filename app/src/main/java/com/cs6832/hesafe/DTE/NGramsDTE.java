package com.cs6832.hesafe.DTE;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Sam on 3/5/16.
 */

public class NGramsDTE extends DTE {
    // The markov model.
    public HashMap<String, LinkedHashMap<String,Double>> MarkovChain;

    // The probability distribution for the starting ngrams.
    public LinkedHashMap<String, Double> InitialMap;

    // The probability of choosing some other character not in the list.
    private final Double EPSILON = 0.001;
    private final Double NUMSUPP = 94.0;
    private final Integer PADLENGTH = 129024;
    private LinkedHashMap<String,Double> validChars = null;
    private final double[] LetterFreq = {.0854,.016,.0316,.0387,.121,.0218,.0209,.0496,.0733,.0022,
                                         .0081,.0421,.0253,.0717,.0747,.0207,.001,.0633,.0673,.0894,
                                         .0268,.0106,.0183,.0019,.0172,.0011};

    public NGramsDTE() {
        validChars = new LinkedHashMap<>();

        for (Character i = 33; i < 127; i++) {
            validChars.put(i.toString(), .5);
        }
    }

    public NGramsDTE(HashMap<String, LinkedHashMap<String,Double>> table,
                     LinkedHashMap<String,Double> init) {
        MarkovChain = table;
        InitialMap = init;

        // Initialize set of characters.
        validChars = new LinkedHashMap<>();

        for (Character i = 33; i < 127; i++) {
            // Capitals Dist.
            if (i >= 65 && i <= 90) {
                validChars.put(i.toString(),((26.0+10.75)/(NUMSUPP+1))*LetterFreq[i-65]);
            }
            else if (i >= 97 && i <= 122) {
                validChars.put(i.toString(),((26.0+10.75)/(NUMSUPP+1))*LetterFreq[i-97]);
            }
            else {
                validChars.put(i.toString(), .5/(NUMSUPP+1));
            }
        }

        validChars.put(new Character((char) 9654).toString(), .5 / (NUMSUPP + 1));
    }

    public HashMap<String, LinkedHashMap<String,Double>> CreateTable(ArrayList<String> input) {
        return null;
    }

    // Iterate through the characters in the password and randomly choose from each interval a seed.
    // Combine the seeds to get the final bitstring.
    public String encode(String password) throws IOException {
        // Create decimal formatter.
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(Integer.MAX_VALUE);
        df.setMinimumFractionDigits(1);
        df.setMaximumIntegerDigits(Integer.MAX_VALUE);
        df.setMinimumIntegerDigits(1);
        df.setGroupingUsed(false);

        // Split the message into ngram tokens.
        String full = ((char)9664 + password + (char)9654);
        String ngram = full.substring(0,4);

        // Get the initial probability for the first ngram.
        String binary = "";
        double first = 0.0;
        double last = 0.0;
        double n = InitialMap.get("sum");
        for (String key : InitialMap.keySet()) {
            if (key.equals(ngram)) {
                last = InitialMap.get(key) + first;
                break;
            }
            else if (key.equals("other")) {
                // Convert the ngram by character to a base ten integer.
                Double sum = 0.0;
                Double j = 0.0;
                for (Character c : ngram.substring(1).toCharArray()) {
                    sum += (c-33)*Math.pow(NUMSUPP, j);
                    j++;
                }


                last = ((sum+1)*n/Math.pow(NUMSUPP,3.0))*EPSILON*EPSILON*EPSILON + first;
                first += (sum*n/Math.pow(NUMSUPP,3.0))*EPSILON*EPSILON*EPSILON;
                break;
            }

            first += InitialMap.get(key);
        }

        // Choose a random value from this range.
        SecureRandom rand = new SecureRandom();
        double next = rand.nextDouble();
        double choice1 = (last)*next + (first)*(1-next);

        // Normalize this probability with respect to the max long size (2^63-1)
        BigDecimal choiceB1 = new BigDecimal(choice1/n).setScale(16, 0);
        BigDecimal newChoice1 = new BigDecimal(Long.MAX_VALUE).setScale(16, 0).multiply(choiceB1);

        // Convert this to bits and cat it.
        binary += BigDtoBinary(df.format(newChoice1));


        // Iterate through the characters in the password (which is a message).
        for (int i = 4; i < full.length(); i++) {
            // Get the character probabilities.
            LinkedHashMap<String,Double> probs = MarkovChain.get(ngram);

            // If the ngram is not in the table, then pick it uniformly from all others chars.
            if (probs == null) {
                probs = new LinkedHashMap<>();
                probs.put("other", 1.0);
            }

            double min = 0.0;
            double max = 0.0;
            // Retrieve the range for this character prob.
            for (String key : probs.keySet()) {
                if (key.equals(full.substring(i, i + 1))) {
                    max = probs.get(key) + min;
                    break;
                }
                // If we didn't find the character, pick from a uniform dist. of all others.
                else if (key.equals("other")) {
                    // Remove all already found characters.
                    LinkedHashMap<String,Double> remain = new LinkedHashMap<>(validChars);
                    remain.keySet().removeAll(probs.keySet());

                    // Iterate through all other valid ascii combs.
                    for (String j : remain.keySet()) {
                        if (j.equals(full.substring(i, i+1))) {
                            max = (probs.get("other")*remain.get(j)) + min;
                            break;
                        }

                        min += probs.get("other")*remain.get(j);
                    }
                    break;
                }

                min += probs.get(key);
            }


            // Choose a random value from this range.
            rand = new SecureRandom();
            Double randD = rand.nextDouble();
            Double choice = max*randD + min*(1-randD);

            // Normalize this probability with respect to the max long size (2^63-1)
            BigDecimal choiceB = new BigDecimal(choice).setScale(16, 0);
            BigDecimal newChoice = new BigDecimal(Long.MAX_VALUE).setScale(16, 0).multiply(choiceB);

            // Convert this to bits and cat it.
            binary += BigDtoBinary(df.format(newChoice));

            // Set the next ngram.
            ngram = ngram.substring(1,4) + full.substring(i,i+1);
        }

        // Add padding to the final binary string.
        if (PADLENGTH < binary.length()) {
            for (int i = 0; i < binary.length()%16; i++) {
                binary += rand.nextInt(2);
            }
            return binary;
        }
        else {
            rand = new SecureRandom();

            int extra = (PADLENGTH-binary.length())%8;
            byte[] r = new byte[(PADLENGTH-binary.length())/8];
            rand.nextBytes(r);
            for (int i = 0; i < extra; i++) {
                binary += rand.nextInt(2);
            }
            System.out.println(binary.length());
            for (int i = 0; i < r.length; i++) {
                binary += String.format("%8s", Integer.toBinaryString(r[i] & 0xFF)).replace(' ', '0');
            }

            return binary;
        }
    }

    // Use the seeds to get a random walk on the markov model.
    public String decode(String seed) {
        // Get the initial ngram.
        Double prob1 = BinarytoBigD(seed.substring(0, 126)).divide(new BigDecimal(Long.MAX_VALUE),16,0).doubleValue();

        String word = "";
        Double end = 0.0;
        double n = InitialMap.get("sum");
        for (String key : InitialMap.keySet()) {
            end += InitialMap.get(key);

            if (prob1 < end/n) {
                // If its some other ngram choose from uniform dist.
                if (key.equals("other")) {
                    Double num = ((EPSILON*EPSILON*EPSILON)-(1-prob1))*
                            (Math.pow(NUMSUPP, 3.0)/(EPSILON*EPSILON*EPSILON));

                    char[] ngram2 = new char[3];
                    for (int i = 0; i < 3; i++) {
                        ngram2[i] = (char) ((new Double(num % NUMSUPP)).intValue() + 33);
                        num = num / NUMSUPP;
                    }

                    word += (char)9664 + new String(ngram2);
                    break;
                }

                word += key;
                break;
            }
        }

        // Iterate over the seed string
        String ngram = word;

        // Create a list of split binary probabilities.
        String[] list = split(seed.substring(126), 126);
        for (String p : list) {
            Double prob = BinarytoBigD(p).divide(new BigDecimal(Long.MAX_VALUE),16,0).doubleValue();

            Double last = 0.0;
            HashMap<String, Double> section = MarkovChain.get(ngram);
            // If this ngram is not in the table, search through a uniform dist. for next char.
            if (section == null) {
                section = new HashMap<>();
                section.put("other", 1.0);
            }
            // Iterate though all keys in the table.
            for (String key : section.keySet()) {

                if (key.equals("other")) {
                    // Remove the already found characters.
                    LinkedHashMap<String,Double> remain = new LinkedHashMap<>(validChars);
                    remain.keySet().removeAll(section.keySet());

                    // Find out which character was used.
                    for (String i : remain.keySet()) {
                        last += section.get("other")*remain.get(i);

                        if (prob < last) {
                            word += i;
                            break;
                        }
                    }
                    break;
                }

                last += section.get(key);

                if (prob < last) {
                    word += key;
                    break;
                }
            }

            // Set it to the next ngram.
            ngram = word.substring(word.length()-4, word.length());

            // We are done if this ngram contains the stop character.
            if (ngram.contains(String.valueOf((char) 9654))) {
                break;
            }
        }

        System.out.println("NGRAMS_DECODE_OUT: " + word);
        return word;
    }

    // Get the popularity of a word. This is simply the probabilities of getting each letter
    // multiplied by the length of the word.
    public Double getPop(String w) {
        if (w.length() < 4) {
            return 0.0;
        }

        String full = ((char)9664 + w + (char)9654);
        String ngram = full.substring(0,4);

        Double n = InitialMap.get("sum");
        Double prob = InitialMap.get(full.substring(0, 4));
        if (prob == null) {
            prob = InitialMap.get("other");
        }
        prob /= n;

        for (int i = 4; i < full.length(); i++) {
            Double prob2 = 1.0;
            HashMap section = MarkovChain.get(ngram);

            if (section == null) {
                prob2 = validChars.get(full.substring(i,i+1));
            }
            else {
                prob2 = (Double) section.get(full.substring(i,i+1));

                if (prob2 == null) {
                    LinkedHashMap<String,Double> remain = new LinkedHashMap<>(validChars);
                    remain.keySet().removeAll(section.keySet());

                    prob2 = EPSILON*remain.get(full.substring(i,i+1));
                }
            }

            ngram = ngram.substring(1,4) + full.substring(i,i+1);
            prob *= prob2;
        }

        return prob*w.length();
    }

    // Convert a Double into a binary string. This method separates the whole and decimal parts,
    // representing them as 64 bit longs.
    public String BigDtoBinary(String d) {
        // Split into whole and fractional strings.
        Long n = Long.parseLong(d.substring(0, d.indexOf(".")));
        String binaryWhole = Long.toBinaryString(n);

        Long frac = Long.parseLong(new StringBuilder(d.substring(d.indexOf(".") + 1)).reverse().toString());
        String binaryFrac = Long.toBinaryString(frac);

        return new String(new char[63-binaryWhole.length()]).replace("\0", "0") + binaryWhole
                + new String(new char[63-binaryFrac.length()]).replace("\0", "0") + binaryFrac;
    }

    // Convert a binary string representation into a double.
    public BigDecimal BinarytoBigD(String s) {
        Long whole = new BigInteger(s.substring(0,63),2).longValue();
        Long frac = new BigInteger(s.substring(63),2).longValue();


        String flip = new StringBuilder(frac.toString()).reverse().toString();

        return new BigDecimal(whole + "." + flip).setScale(16,0);
    }

    private String[] split(String s, int size) {
        List<String> out = new ArrayList<>((s.length() + size - 1) / size);

        for (int begin = 0; begin < s.length(); begin += size) {
            out.add(s.substring(begin, begin + size));
        }

        return out.toArray(new String[0]);
    }


    // Test main.
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileInputStream file = new FileInputStream("chain.ser");
        ObjectInputStream in = new ObjectInputStream(file);

        FileInputStream file2 = new FileInputStream("init.ser");
        ObjectInputStream in2 = new ObjectInputStream(file2);

        HashMap<String, LinkedHashMap<String,Double>> table =
                (HashMap<String, LinkedHashMap<String,Double>>) in.readObject();

        LinkedHashMap<String, Double> init = (LinkedHashMap<String, Double>) in2.readObject();

        NGramsDTE DTE = new NGramsDTE(table, init);

        // Generate bit strings at random and print them.
        ArrayList<String> example = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            Random rand = new Random();
            int length = rand.nextInt(12)+10;

            String bits3 = "";
            for (int j = 0; j < length; j++) {
                for (int k = 0; k < 126; k++) {
                    bits3 += rand.nextInt(2);
                }

            }

            example.add(bits3);
        }

//        for (String s : example) {
//            //System.out.println(s);
//            String word = DTE.decode(s);
//            System.out.println(word);
//        }

        String bit = DTE.encode("Hello_myT#");
        System.out.println(bit);
        System.out.println(bit.length());
        System.out.println(DTE.decode(bit));
    }
}
