package com.cs6832.hesafe;/*utils class to define useful helper functions
 */
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.cs6832.hesafe.hesafe.R;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Iterator;

public class Utils{
	public static byte[] concat(byte[] a, byte[] b){
        	int n = a.length + b.length;
                byte[] tmp = new byte[n];
                for(int i= 0; i<a.length; i++){
                        tmp[i] = a[i];
                }
                for(int j=a.length; j<n; j++){
                        tmp[j] = b[j-a.length];
                }
		return tmp;
        }
	public static byte[] Xor(byte[] a, byte[] b){
		int n = Math.min(a.length, b.length);
		int N = Math.max(a.length, b.length);

		byte[] tmp = new byte[n];
		for(int i=0; i<	n; i++){//fill it until one of them is done entirely
			int tmpA = (int)a[i];
			int tmpB = (int)b[i];
			byte xored = (byte)(tmpA ^ tmpB);
			tmp[i] = (byte)(xored);
		}
		if(a.length> n){//case where a is the bigger one
			for(int j=a.length; j<N; j++){
				tmp[j] = a[j];
			}
		}else{
			for(int j=b.length; j<N;j++){
				tmp[j] = b[j];
			}
		}
		return tmp;
	}
			
	/*pad[a,n] will pad 0's into byte[] a until a.length = n
	Precondition: a.length < n
	Postconditon: a.length = n and the added extra bits are all zero
	*/
	public static byte[] pad(byte[] a, int n){
		byte[] padded = new byte[n];
		for(int i = 0; i< a.length; i++){
			padded[i] = a[i];
		}
		for(int j = a.length; j< n; j++){
			padded[j] = 0;
		}
		return padded;		
	}
	public static byte[] getBytes(String a){
		try{
			byte[] b = a.getBytes("UTF-8");
		
			return b;

		}catch(Exception e){
			System.out.println("error has occured");
			return null;
		}
	}
	public static String getString(byte[] a){
		String str = new String(a); 
		return str;
	}
	public static boolean isEqual(byte[] a, byte[] b){
		if(a.length != b.length){
			return false;
		}
		for(int i =0; i < a.length; i++){
			if( a[i] != b[i]) {
				return false;
			}
		}
		return true;
	}
	public static void printBytes(byte[] a){
		for(int i=0; i<a.length; i++){
			System.out.print(a[i]);
			System.out.print("  ");
		}
	}
    /*this class will take in a binary bitstring (e.g "010100101") such that string.length | 8 and
    return a byte[] representation for it (e.g "011111111" -> [0b01111111] )
     */
	public static byte[] getBytesFromBinaryString(String a) throws Exception{
		char[] chars = a.toCharArray();

        byte[] res = new byte[a.length()/8];
        int k = 0;
        for(int j= 0; j<chars.length; j= j + 8) {
            byte mask = (byte)0x01;
            res[k] = (byte)(((chars[j] & mask) << 7) | ((chars[j+1] & mask) << 6) | ((chars[j+2] & mask) << 5) | ((chars[j+3] & mask) << 4) | ((chars[j+4] & mask) << 3) | ((chars[j+5] & mask) << 2) | ((chars[j+6] & mask) << 1) | ((chars[j+7] & mask)));
            k++;
        }
        return res;
	}
	public static String getBinaryStringfromBytes(byte[] a){
        StringBuffer res = new StringBuffer();
        for(byte by : a){
            res.append((by & 0b10000000) >> 7);
            res.append((by & 0b01000000) >> 6);
            res.append((by & 0b00100000) >> 5);
            res.append((by & 0b00010000) >> 4);
            res.append((by & 0b00001000) >> 3);
            res.append((by & 0b00000100) >> 2);
            res.append((by & 0b00000010) >> 1);
            res.append((by & 0b00000001));
        }
        return res.toString();
	}

}
