package com.cs6832.hesafe;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import com.cs6832.hesafe.DTE.DTE;
import com.cs6832.hesafe.DTE.NGramsDTE;
import com.cs6832.hesafe.hesafe.R;
import com.cs6832.hesafe.manager.Manager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class LoginActivity extends AppCompatActivity {
    public final static String VAULT_MESSAGE = "com.cs6832.hesafe.VAULT_NAME";
    public final static String PASS_MESSAGE = "com.cs6832.hesafe.PASSWORD_NAME";
    Manager manager;
    NGramsDTE NGrams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Read the serialized files and initialize the DTE.
        if (NGrams == null) {
            HashMap<String, LinkedHashMap<String,Double>> table = null;
            LinkedHashMap<String, Double> init = null;
            try {

                InputStream fis = getResources().openRawResource(R.raw.chain);
                ObjectInputStream in = new ObjectInputStream(fis);

                InputStream fis2 = getResources().openRawResource(R.raw.init);
                ObjectInputStream in2 = new ObjectInputStream(fis2);

                table = (HashMap<String, LinkedHashMap<String,Double>>) in.readObject();

                init = (LinkedHashMap<String, Double>) in2.readObject();

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (table == null || init == null) {
                Toast.makeText(this, "The vault failed to load due to missing resource files.",
                        Toast.LENGTH_SHORT).show();

            }

            NGrams = new NGramsDTE(table, init);
        }


        manager = new Manager(this);
    }

    // Method that switches the activity when the login button is used.
    public void login(View view) {
        Intent intent = new Intent(this, VaultDisplay.class);
        // Get the vault name and password from the user.
        EditText vaultText = (EditText) findViewById(R.id.vaultName);
        EditText passText = (EditText) findViewById(R.id.password);
        String vault_name = vaultText.getText().toString();
        String password = passText.getText().toString();

        // Perform Typo checking.
        password = UtilityTest.word_center(password, NGrams);

        // Add key value pairs to send to the next activity.
        intent.putExtra(VAULT_MESSAGE, vault_name);
        intent.putExtra(PASS_MESSAGE, password);

        System.out.println(vault_name);
        System.out.println(passText);

        // Check to see if this is a valid vault.
        if (manager.logIn(vault_name, this)) {
            startActivity(intent);
        }
        else {
            Toast.makeText(this,"The vault does not exist.",Toast.LENGTH_LONG).show();
        }
    }


    /*Method that switched the activity when the create account button is pressed
      */
    public void registerNewAccount(View view) {
        //begin the Create Account Activity
        System.out.println("button presses!");
        Intent intent = new Intent(this, CreateAccountActivity.class);
        startActivity(intent);
    }


}
