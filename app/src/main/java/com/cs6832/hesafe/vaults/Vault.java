package com.cs6832.hesafe.vaults;

import android.content.Context;

import com.cs6832.hesafe.DTE.DTE;
import com.cs6832.hesafe.DTE.NGramsDTE;
import com.cs6832.hesafe.LoginActivity;
import com.cs6832.hesafe.Utils;
import com.cs6832.hesafe.crypto.HCipher;
import com.cs6832.hesafe.crypto.PBE;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;


/* The layout for a vault file is below:
    1) the password salt             (256 bytes)
    2) the iv vector for the key     (16 bytes)
    3) the encrypted key             (256 bytes)
    4) the iv vector for the picture (16 bytes)
    5) the encrypted picture         (32 bytes)
    6) the iv vector for the encrypted password (16 bytes)
    7) the encrypted password.       (16128 bytes)
    8) all the other iv,password pairs...
*/
public class Vault {
    public Context activity;
    private String vault_name;
    private String mpw;
    private int picture_id = -1;
    private NGramsDTE ngrams;
    private PBE pbe;

    private final int SALT_SIZE  = 256*8;
    private final int IV_SIZE    = 16*8;
    private final int KEY_SIZE   = 256*8;
    private final int BLOCK_SIZE = 16128*8;
    private final int PIC_SIZE   = 32*8;

    public Vault(String name, String password, NGramsDTE dte, Context context) {
        activity = context;
        vault_name = name;
        mpw = password;
        ngrams = dte;
    }

    public void addPassword(String newpass) {
        try {
            FileOutputStream fos = activity.openFileOutput(vault_name, activity.MODE_APPEND);
            OutputStreamWriter osw = new OutputStreamWriter(fos);

            ArrayList<byte[]> cipher = pbe.encryptPass(newpass);
            System.out.println("IV: " + Utils.getBinaryStringfromBytes(cipher.get(0)));
            System.out.println("PASS: " + Utils.getBinaryStringfromBytes(cipher.get(1)));
            osw.write(Utils.getBinaryStringfromBytes(cipher.get(0)));
            osw.write(Utils.getBinaryStringfromBytes(cipher.get(1)));
            osw.close();
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*getPassword[mpw] will run a decryption algorithm on the encrypted password list
    and return a list of decrypted passwords
     */
    public ArrayList<String> getPasswords() {
        ArrayList<String> passList = new ArrayList<>();
        try {
            // Open the vault password file and read from it.
            FileInputStream fis = activity.openFileInput(vault_name);
            InputStreamReader isr = new InputStreamReader(fis);

            int len = 0;
            char[] saltIn = new char[SALT_SIZE];
            char[] keyIn = new char[IV_SIZE+KEY_SIZE];
            char[] picIn = new char[IV_SIZE+PIC_SIZE];
            char[] passIn = new char[IV_SIZE+BLOCK_SIZE];
            try {
                // Read the salt.
//                byte[] salt;
//                if((len=isr.read(saltIn)) > 0) {
//                    String saltData = String.copyValueOf(saltIn, 0, SALT_SIZE);
//                    salt = Utils.getBytesFromBinaryString(saltData);
//                }

                // Initialize the PBE.
                pbe = new PBE(mpw);
                pbe.setNgrams(ngrams);

                if((len=isr.read(keyIn)) > 0) {
                    String ivData = String.copyValueOf(keyIn,0,IV_SIZE);
                    String keyData = String.copyValueOf(keyIn, IV_SIZE, KEY_SIZE);
                    byte[] iv = Utils.getBytesFromBinaryString(ivData);
                    byte[] key = Utils.getBytesFromBinaryString(keyData);

                    ArrayList<byte[]> secret = new ArrayList<>();
                    secret.add(iv);
                    secret.add(key);

                    // Decrypt the secret key.
                    pbe.setPrivateKey(secret);
                }

                // Now read the picture data.
                if((len=isr.read(picIn)) > 0) {

                }

                // Now read all of the vault data.
                while((len=isr.read(passIn)) > 0) {
                    String ivData = String.copyValueOf(passIn,0,IV_SIZE);
                    String passData = String.copyValueOf(passIn, IV_SIZE, BLOCK_SIZE);
                    byte[] iv = Utils.getBytesFromBinaryString(ivData);
                    byte[] pass = Utils.getBytesFromBinaryString(passData);
                    System.out.println("iv: " + Utils.getBinaryStringfromBytes(iv));
                    System.out.println("pass: " + Utils.getBinaryStringfromBytes(pass));

                    ArrayList<byte[]> passCipher = new ArrayList<>();
                    passCipher.add(iv);
                    passCipher.add(pass);

                    String newPass = new String(pbe.decryptPass(passCipher), "UTF-8");
                    passList.add(newPass.substring(1,newPass.length()-1));
                    System.out.println(passList);
                }

                isr.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Now decrypt the list of passwords using our Cipher classes.
        return passList;
    }

    // Reads the picture from the file and decrypts it.
    public int getPicture() {
        if (picture_id > 0) {
            return picture_id;
        }

        try {
            // Open the vault password file and read from it.
            FileInputStream fis = activity.openFileInput(vault_name);
            InputStreamReader isr = new InputStreamReader(fis);

            int len = 0;
            char[] saltIn = new char[SALT_SIZE];
            char[] keyIn = new char[IV_SIZE+KEY_SIZE];
            char[] picIn = new char[IV_SIZE+PIC_SIZE];
            try {
                // Read the salt.
//                byte[] salt;
//                if((len=isr.read(saltIn)) > 0) {
//                    String saltData = String.copyValueOf(saltIn, 0, SALT_SIZE);
//                    salt = Utils.getBytesFromBinaryString(saltData);
//                }

                // Initialize the PBE.
                PBE pbe = new PBE(mpw);

                if((len=isr.read(keyIn)) > 0) {
                    String ivData = String.copyValueOf(keyIn,0,IV_SIZE);
                    String keyData = String.copyValueOf(keyIn, IV_SIZE, KEY_SIZE);
                    byte[] iv = Utils.getBytesFromBinaryString(ivData);
                    byte[] key = Utils.getBytesFromBinaryString(keyData);

                    ArrayList<byte[]> secret = new ArrayList<>();
                    secret.add(iv);
                    secret.add(key);

                    // Decrypt the secret key.
                    pbe.setPrivateKey(secret);
                }

                // Now read the picture data.
                if((len=isr.read(picIn)) > 0) {
                    String ivData = String.copyValueOf(picIn,0,IV_SIZE);
                    String picData = String.copyValueOf(picIn,IV_SIZE,PIC_SIZE);
                    byte[] iv = Utils.getBytesFromBinaryString(ivData);
                    byte[] pic = Utils.getBytesFromBinaryString(picData);

                    ArrayList<byte[]> picCipher = new ArrayList<>();
                    picCipher.add(iv);
                    picCipher.add(pic);

                    picture_id = pbe.decryptPicture(picCipher);
                }
                isr.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return picture_id;
    }

    public void chagePassword() {

    }

    public void deleteVault() {

    }

}
