package com.cs6832.hesafe.manager;
import android.content.Context;

import com.cs6832.hesafe.DTE.NGramsDTE;
import com.cs6832.hesafe.Utils;
import com.cs6832.hesafe.crypto.HCipher;
import com.cs6832.hesafe.vaults.Vault;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.security.SecureRandom;
import java.util.ArrayList;

import com.cs6832.hesafe.DTE.*;
import com.cs6832.hesafe.crypto.PBE;
/**
 * Created by oaz2 on 5/6/16.
 */
public class Manager{
    Context activity;

    public Manager(Context context){
        activity = context;
    }

    /*login[name, password] returns the vault with name = [name] and mpw=[Password]
    if [name] is not valid, throws Exception
     */
    public Boolean logIn(String vaultName, Context context) {
        File vault = context.getFileStreamPath(vaultName);
        if (vault == null || !vault.exists()) {
            return false;
        }

        return true;
    }


    public boolean isVault(String vaultName, Context context){
        File vault  = context.getFileStreamPath(vaultName);
        if(vault == null || !vault.exists()){
            return false;
        }
        else{
            return true;
        }
    }


    /*create[vaultName] [pass] [pic] will create a key for the vault.
    Preconditon: isVault[vaultname] == false.  Otherwise causes undefined behavoir
    Postcondition: Will create a file called [vaultName] that contains (in order)
        1) the iv vector for the key
        2) the encrypted key
        3) the iv vector for the picture
        4) the encrypted picture
     */
    public void create(String vaultName, String password, Integer pic) {
        // Encrypt the picture number and write it to a new vault file.

        //store both of these values into the file
        try {
            System.out.println(password);
            PBE pbe = new PBE(password);
            ArrayList<byte[]> newKey = pbe.generateNewKey();
            pbe.setPrivateKey(newKey);
            ArrayList<byte[]> encPic = pbe.encryptPicture(pic);
            FileOutputStream fos = activity.openFileOutput(vaultName, activity.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            System.out.println("IV: " + Utils.getBinaryStringfromBytes(encPic.get(0)));
            System.out.println("KEY: " + Utils.getBinaryStringfromBytes(encPic.get(1)));
            osw.write(Utils.getBinaryStringfromBytes(newKey.get(0)));//store the encrypted key and iv
            osw.write(Utils.getBinaryStringfromBytes(newKey.get(1)));
            osw.write(Utils.getBinaryStringfromBytes(encPic.get(0)));//store the encrypted picture and iv
            osw.write(Utils.getBinaryStringfromBytes(encPic.get(1)));
            osw.close();
            fos.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        //generate a random key
        //encrypt the key using the Ciphers
        //encrypt the picture number as well? (maybe)
    }

}
