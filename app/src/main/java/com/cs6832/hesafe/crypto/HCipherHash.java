package com.cs6832.hesafe.crypto;
import com.cs6832.hesafe.Utils;

import com.cs6832.hesafe.DTE.DTE;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.security.*;


public class HCipherHash{
	private MessageDigest digest;
//	private SecretKey key;
//	private SecretKeyFactory kFact;
	private SecureRandom rand;
	private DTE dte;
	private byte[] bkey;

	public HCipherHash(byte[] mpw, DTE dte){
		this.dte = dte; 
		try{
			digest = MessageDigest.getInstance("SHA-256");
//		kFact = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
 	        	rand = SecureRandom.getInstance("SHA1PRNG");
			bkey = mpw;
        	}catch(Exception e){
			System.out.println("Error has occured");
		}        
	//      System.out.println("works!");
    //            PBEKeySpec spec = new PBEKeySpec(mpw, salt, 1024, 256);
                //      System.out.println("works!2");
      //          key = new SecretKeySpec(kFact.generateSecret(spec).getEncoded(),"AES");				
	//	bkey = key.getEncoded();
	
	}
	public ArrayList<byte[]> HEnc(String plaintext){
		try{
		System.out.println("Begin encryption of text: " + plaintext);
		String encoded  = dte.encode(plaintext);
		
		System.out.println("encoded string: " + encoded);

		byte[] encBytes = Utils.getBytes(encoded);
		System.out.println("size of encBytes is: " + encBytes.length);
		
		int n = bkey.length;
		byte[] randArr = new byte[256-n];
		rand.nextBytes(randArr);
		byte[] tmp = Utils.concat(bkey, randArr);//com.cs6832.mccoymattsam.Utils.concat into new byte[] use this to encrypt
		byte[] hash = digest.digest(tmp);
		System.out.println("size of hash[] is: "+ hash.length);

		ArrayList<byte[]> tuple = new ArrayList<byte[]>(2);
		byte[] cipher = Utils.Xor(encBytes, hash); //h(r,k)xor m
		tuple.add(0,randArr);
		tuple.add(1,cipher); //return (r,h(k,r)xor m)
		return tuple;
		}catch(Exception e){
			System.out.println("An error has occured in HEnc");
			e.printStackTrace();
			assert(false);
			return null;
		}

	}
	public byte[] HDec(ArrayList<byte[]> Ctuple){
		try{
		byte[] randArr = Ctuple.get(0);
		byte[] cipher = Ctuple.get(1);
		byte[] hash = digest.digest(Utils.concat(bkey,randArr));
		byte[] S = Utils.Xor(cipher, hash);
		String SEnc = Utils.getString(S);
		System.out.println("decrypted S: " + SEnc);

		byte[] plaintext  = Utils.getBytes(dte.decode(SEnc));
		return plaintext;
		}catch(Exception e){
			System.out.println("An error has occured in HDec");
			e.printStackTrace();
			assert(false);
			return null;
		}

	}
}
