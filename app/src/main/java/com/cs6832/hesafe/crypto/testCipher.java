package com.cs6832.hesafe.crypto;

import com.cs6832.hesafe.DTE.DTE;
import com.cs6832.hesafe.DTE.NGramsDTE;
import com.cs6832.hesafe.Utils;

import java.util.Scanner;
import java.util.*;
import java.io.*;

public class testCipher{
	public static void main(String[] args) throws Exception{
		//init Ngrams
		System.out.println("Starting");

		FileInputStream file = new FileInputStream("chain.ser");
		ObjectInputStream in = new ObjectInputStream(file);

		FileInputStream file2 = new FileInputStream("init.ser");
		ObjectInputStream in2 = new ObjectInputStream(file2);

		HashMap<String, LinkedHashMap<String,Double>> table = (HashMap<String, LinkedHashMap<String,Double>>) in.readObject();
		LinkedHashMap<String, Double> init = (LinkedHashMap<String, Double>) in2.readObject();

		System.out.println("Making");
		DTE ngram = new NGramsDTE(table, init);
		System.out.println("Made correct ngram");

//		DTE ngram = new DummyDTE();
			
		while(true){
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter master password");
			String pass = sc.nextLine();
			System.out.println("Enter incorrect password");
			String fakepass = sc.nextLine();
			System.out.println("Enter message to encrypt");
			String message = sc.next();
			String salt = "123";
			HCipher goodEnc = new HCipher(pass.toCharArray(),Utils.getBytes(salt), ngram);
			HCipher badEnc = new HCipher(fakepass.toCharArray(),Utils.getBytes(salt), ngram);


			ArrayList<byte[]> cipher = goodEnc.HEnc(message);//encrypt message
			byte[] ciphertxt = cipher.get(1);
			byte[] iv = cipher.get(0);

			while(true){
				System.out.println("Enter 1 to decrypt with correct pass, 2 to decrypt with incorrect pass and 0 to end simulation");
				int i = sc.nextInt();
				if(i == 1){
					System.out.println("Decrypting with correct password");
					byte[] plaintxt = goodEnc.HDec(iv, ciphertxt);
					System.out.println(new String(plaintxt));
				}
				if(i == 2){
					System.out.println("Decrypting with incorrect password");
					byte[] plaintxt = badEnc.HDec(iv, ciphertxt);
					System.out.println(new String(plaintxt));
				
				}
				if(i == 0){
					break;
				}
			}
		}
	}
}	
