package com.cs6832.hesafe.crypto;
import java.security.SecureRandom;
import javax.crypto.*;
import java.security.NoSuchAlgorithmException;
/* create by Oscar*/

public class PBCipher{
	private SecureRandom rand;
	private SecretKeyFactory keyFact;	
	public PBCipher(){
		System.out.println("init");
		try{
			keyFact = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			rand = SecureRandom.getInstance("SHA1PRNG");			
			byte[] salt = getSalt(10);
			
		} catch(NoSuchAlgorithmException e) {
			System.out.print("Invalid algorithm");
		}
	}
	public byte[] HEnc(SecretKey k, byte[] M){
		return null;
	}

	public byte[] HDec(SecretKey k, byte[] C){
		return null;
	}
	/*getSalt[n] returns a salt of size n */
	public byte[] getSalt(int n){
		byte[] tmpB = new byte[n];
		rand.nextBytes(tmpB);
		return tmpB;
	}
}

