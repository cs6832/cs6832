package com.cs6832.hesafe.crypto;

/*This is the class that will perform the 2key encryption algorithm
 */
import com.cs6832.hesafe.DTE.DummyDTE;
import com.cs6832.hesafe.Utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import com.cs6832.hesafe.DTE.*;
import java.util.ArrayList;

import javax.crypto.AEADBadTagException;

public class PBE{
	private HCipher h1;
	private PicDTE picDTE;
	private NGramsDTE nGramsDTE;
	private SecureRandom rand; //needs to be a place where the ngram stuff is initiatiated!
	private byte[] randKey;
	private boolean keySet;
	private byte[] salt = {1,2,3}; //constant salt for now, it shouldn't matter

	public PBE(String mpw){
		try {
			keySet = false;
			rand = SecureRandom.getInstance("SHA1PRNG");
			picDTE = new PicDTE();
			nGramsDTE = new NGramsDTE(); //TODO: INITIALIZE IT!
			h1 = new HCipher(mpw.toCharArray(), salt, new DummyDTE());
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	/*encryptPass[plainText] will return (iv, cipher)
	Precondition: keySet == true
	Postcondition : decryptPass(encryptPass(cipher)) == cipher w.p 1 and cipher satisfies the
	honey encryption defintion of security
	 */
	public ArrayList<byte[]> encryptPass(String plaintext) throws Exception {
		if (!keySet) {
			throw new Exception("Private key not set");
		}
		try {
			h1.setDTE(new DummyDTE());
			HCipher h2 = new HCipher(randKey, salt, nGramsDTE);
			return h2.HEnc(plaintext);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public byte[] decryptPass(ArrayList<byte[]> cipherText) throws Exception{
		if (!keySet){
			throw new Exception("Private key not set");
		}
		try{
			HCipher h2 = new HCipher(randKey, salt, nGramsDTE);
			return h2.HDec(cipherText.get(0), cipherText.get(1));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	/*encryptPicture[id] will take encrypt the picture ID using the PicDTE Honey Encryption
	Precondition: keySet == true
	Postconditon: decryptPicture[encryptPicture[id]] == id
	 */
	public ArrayList<byte[]> encryptPicture(int picId) throws Exception{
		if(!keySet){
			throw new Exception("Private key not set");
		}
		HCipher h2;
		try {
			h2 = new HCipher(randKey, salt, picDTE);
			return h2.HEnc(String.valueOf(picId));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	public int decryptPicture(ArrayList<byte[]> cipherText) throws Exception{
		if(!keySet){
			throw new Exception("Private key not set");
		}
		HCipher h2;
		try {
			h2 = new HCipher(randKey, salt, picDTE);
			byte[] out = h2.HDec(cipherText.get(0),cipherText.get(1));
			System.out.println("out: " + Utils.getBinaryStringfromBytes(out));

			byte[] fin = new byte[4];
			fin[0] = 0;
			fin[1] = 0;
			fin[2] = 0;
			fin[3] = out[0];

			System.out.println(Utils.getBinaryStringfromBytes(fin));
			ByteBuffer bb = ByteBuffer.wrap(fin);
			return bb.getInt();
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	/* this method must be called when before trying to run any of the encryption/decryption algorithms
	 */
	public void setPrivateKey(ArrayList<byte[]> cipherKey){
		try {
			h1.setDTE(new DummyDTE());
			randKey = h1.HDec(cipherKey.get(0), cipherKey.get(1));
			keySet = true;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private byte[] getSalt(int n){
		byte[] tmpB = new byte[n];
		SecureRandom rand = new SecureRandom();

		rand.nextBytes(tmpB);
		return tmpB;
	}

	/*generateNewKey() returns an encrypted random 256 bit key to be used when decrypting
	 */
	public ArrayList<byte[]> generateNewKey() {
		System.out.println("New Key Generated.");
		try {
			byte[] newKey = new byte[256];
			rand.nextBytes(newKey);
			h1.setDTE(new DummyDTE());
			return h1.HEnc(Utils.getBinaryStringfromBytes(newKey));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setNgrams(NGramsDTE ngrams) {
		nGramsDTE = ngrams;
	}
}
