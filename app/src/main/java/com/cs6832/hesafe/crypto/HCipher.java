package com.cs6832.hesafe.crypto;/*abstract class for the Honey Encryption Ciphers*/

import com.cs6832.hesafe.DTE.DTE;
import com.cs6832.hesafe.DTE.NGramsDTE;
import com.cs6832.hesafe.Utils;

import java.util.*;
import javax.crypto.Cipher;
import java.security.SecureRandom;
import java.security.AlgorithmParameters;

import javax.crypto.SecretKeyFactory;
import javax.crypto.SecretKey;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class HCipher{
	private SecureRandom rand;
	private SecretKeyFactory kFact;
	private DTE dte;
	private Cipher AESCipher;
	private SecretKey key;

	public HCipher(char[] mpw, byte[] salt, DTE dte){
		this.dte = dte;
		try{
//			System.out.println(mpw);
//			System.out.println(salt);
			kFact = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			rand = SecureRandom.getInstance("SHA1PRNG");
			AESCipher = Cipher.getInstance("AES/CBC/NoPadding");


		//	System.out.println("works!");
			System.out.println(new String(mpw));
			System.out.println(Utils.getBinaryStringfromBytes(salt));
			PBEKeySpec spec = new PBEKeySpec(mpw, salt, 1024, 128);
		//	System.out.println("works!2");
			key = new SecretKeySpec(kFact.generateSecret(spec).getEncoded(),"AES");
			System.out.println("new_Key: " + Utils.getBinaryStringfromBytes(key.getEncoded()));
		//	System.out.println("works3");
		}catch(Exception e){
			System.out.println("no such algorith");
		}
	}
	public HCipher(byte[] mpw, byte[] salt, DTE dte){
		this.dte = dte;
		try{
//			System.out.println(mpw);
//			System.out.println(salt);
			kFact = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			rand = SecureRandom.getInstance("SHA1PRNG");
			AESCipher = Cipher.getInstance("AES/CBC/NoPadding");


			//	System.out.println("works!");
			PBEKeySpec spec = new PBEKeySpec(Utils.getString(mpw).toCharArray(), salt, 1024, 128);
			//	System.out.println("works!2");
			key = new SecretKeySpec(kFact.generateSecret(spec).getEncoded(),"AES");
			//	System.out.println("works3");
		}catch(Exception e){
			System.out.println("no such algorith");
		}


	}
	/*Henc[k,m] will return c such that Hdec[k,HEnc[k,m]] = m
	 */
	public ArrayList<byte[]> HEnc(String message) throws Exception{

		ArrayList<byte[]> res = new ArrayList<byte[]>(2);
		AESCipher.init(Cipher.ENCRYPT_MODE ,key);
		AlgorithmParameters params = AESCipher.getParameters();
		byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
        String encoded = dte.encode(message); //this will produce a binary bit string
        System.out.println(encoded);
		System.out.println(dte.decode(encoded));
        //Utils.printBytes(Utils.getBytesFromBinaryString(encoded));
		byte[] ctxt = AESCipher.doFinal(Utils.getBytesFromBinaryString(encoded));

		res.add(0,iv);
		res.add(1,ctxt);
		return res;
	}

	public byte[] HDec(byte[] iv, byte[] cipher) throws Exception{
		AESCipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		System.out.println("decrypting");
        String decrypted = Utils.getBinaryStringfromBytes(AESCipher.doFinal(cipher));
        System.out.println(decrypted); //should print out a binary string
		System.out.println("decoded: " + dte.decode(decrypted));
		if (dte.getClass().equals(NGramsDTE.class)) {
			return dte.decode(decrypted).getBytes("UTF-8");
		}
		else {
			return Utils.getBytesFromBinaryString(dte.decode(decrypted));
		}
	}
	/*sets the DTE to anything we want
	 */
	public void setDTE(DTE dte){
		this.dte = dte;
	}

	/*public abstract byte[] HDec(SecretKey k, byte[] cipher);
	*/
}

