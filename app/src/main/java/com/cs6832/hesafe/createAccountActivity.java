package com.cs6832.hesafe;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.NinePatch;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cs6832.hesafe.DTE.NGramsDTE;
import com.cs6832.hesafe.hesafe.R;

import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Array;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.cs6832.hesafe.manager.Manager;

public class CreateAccountActivity extends AppCompatActivity {
    public final static String VAULT_MESSAGE = "com.cs6832.hesafe.VAULT_NAME";
    public final static String PASS_MESSAGE = "com.cs6832.hesafe.PASSWORD_NAME";
    Manager manager;
    public NGramsDTE NGrams;
    private GridView gridView;
    private GridViewAdapter gridAdapter;
    private ImageItem lastClickedImage; //save the last clicked image, null if no image has been clicked
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        manager = new Manager(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
            /*Im not sure if this goes here*/
        gridView = (GridView) findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, getPictures());
        gridView.setAdapter(gridAdapter);

        // Read the serialized files and initialize the DTE.
        if (NGrams == null) {
            HashMap<String, LinkedHashMap<String,Double>> table = null;
            LinkedHashMap<String, Double> init = null;
            try {

                InputStream fis = getResources().openRawResource(R.raw.chain);
                ObjectInputStream in = new ObjectInputStream(fis);

                InputStream fis2 = getResources().openRawResource(R.raw.init);
                ObjectInputStream in2 = new ObjectInputStream(fis2);

                table = (HashMap<String, LinkedHashMap<String,Double>>) in.readObject();

                init = (LinkedHashMap<String, Double>) in2.readObject();

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (table == null || init == null) {
                Toast.makeText(this, "The vault failed to load due to missing resource files.",
                        Toast.LENGTH_SHORT).show();

            }

            NGrams = new NGramsDTE(table, init);
        }


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageItem item = (ImageItem) parent.getItemAtPosition(position);
                lastClickedImage = item;
            }
        });
    }
    /* when this button is pressed, get the data and invoke the manager? class
       TODO: This button needs to start a new activity
     */
    public void createAccount(View view) {
        if(lastClickedImage == null){
            //TODO: include some logic here to handle this case
            System.out.println("please select a picture!");
        }
        else { //should send me to the log in screen
            EditText vaultNameText = (EditText) findViewById(R.id.prompt_vault_name);
            //actually not sure how secure this is
            EditText passText = (EditText) findViewById(R.id.prompt_pass);
            String vaultName = vaultNameText.getText().toString();
            String pass = passText.getText().toString();

            // Perform typo checking.
            pass = UtilityTest.word_center(pass, NGrams);
            int picId = Integer.parseInt(lastClickedImage.getTitle().substring(1));

            if(!manager.isVault(vaultName, this)){
                //vault name does not exists so we can use it
                manager.create(vaultName, pass, picId); //will create a new account
                Intent intent = new Intent(this, VaultDisplay.class);
                intent.putExtra(VAULT_MESSAGE, vaultName);
                intent.putExtra(PASS_MESSAGE, pass);
                startActivity(intent);
            }
            else{
                Toast.makeText(this,"The vault already exists.",Toast.LENGTH_LONG).show();
            }
        }
    }

    /*moreImages on button press will return a new random subset of the 256 images available
     */
    public void moreImages(View view){
        lastClickedImage = null;
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, getPictures());
        gridView.setAdapter(gridAdapter);
    }
    /*returns the list of pictures in a random order*/
    private ArrayList<ImageItem> getPictures(){
        ArrayList<ImageItem> tmp = new ArrayList<ImageItem>();
        TypedArray imgs = getResources().obtainTypedArray(R.array.pic_ids);
        SecureRandom rand;
        try{
            rand = SecureRandom.getInstance("SHA1PRNG");
        }catch(NoSuchAlgorithmException e){
            e.printStackTrace();
            rand= null;
        }
        int i = 32;
        boolean[] isHit = new boolean[256];
        while(i>0){
            int rint = (Math.abs(rand.nextInt()) % 256);
            if(!isHit[rint]) {
                isHit[rint] = true;
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(rint, -1));
                tmp.add(new ImageItem(bitmap, "p" + rint));
                i--;
            }
        }
        return tmp;
    }

}
