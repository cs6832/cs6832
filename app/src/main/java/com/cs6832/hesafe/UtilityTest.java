package com.cs6832.hesafe;



import com.cs6832.hesafe.DTE.NGramsDTE;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;

/**
 * Created by mccoymattsam on 4/25/16.
 */
public class UtilityTest {
    // Create the different corrector functions that will be applied to create balls.
    public static String swc_all(String w) {
        String upd = "";
        for(Character c : w.toCharArray()) {
            if (Character.isLowerCase(c)) {
                upd += (Character.toUpperCase(c));
            } else if (Character.isUpperCase(c)) {
                upd += (Character.toLowerCase(c));
            } else {
                upd += c;
            }
        }

        return upd;
    }

    public static String swc_first(String w) {
        if (w.matches("^([A-Z]).+$")) {
            return w.substring(0,1).toLowerCase() + w.substring(1);
        }
        else if (w.matches("^([a-z]).+$")) {
            return w.substring(0,1).toUpperCase() + w.substring(1);
        }

        return w;
    }

    public static String rm_last(String w) {
        return w.substring(0,w.length()-1);
    }

    public static String rm_first(String w) {
        return w.substring(1);
    }

    public static String n2s_last(String w) {
        // Guess we enumerate the shift conversions...
        String change = w.substring(w.length()-1);
        if (w.matches("^.+([A-Z])$")) {
            return w.substring(0,w.length()-1) + w.substring(w.length()-1).toLowerCase();
        }
        else if (w.matches("^.+([a-z])$")) {
            return w.substring(0,w.length()-1) + w.substring(w.length()-1).toUpperCase();
        }
        else {
            switch (w.substring(w.length()-1)) {
                case "1": change = "!"; break; case "2": change = "@"; break;
                case "3": change = "#"; break; case "4": change = "$"; break;
                case "5": change = "%"; break; case "6": change = "^"; break;
                case "7": change = "&"; break; case "8": change = "*"; break;
                case "9": change = "("; break; case "0": change = ")"; break;
                case "`": change = "~"; break; case "-": change = "_"; break;
                case "=": change = "+"; break; case "[": change = "{"; break;
                case "]": change = "}"; break; case "\\": change = "|"; break;
                case ";": change = ":"; break; case "'": change = "\""; break;
                case ",": change = "<"; break; case ".": change = ">"; break;
                case "/": change = "?"; break;
                case "!": change = "1"; break; case "@": change = "2"; break;
                case "#": change = "3"; break; case "$": change = "4"; break;
                case "%": change = "5"; break; case "^": change = "6"; break;
                case "&": change = "7"; break; case "*": change = "8"; break;
                case "(": change = "9"; break; case ")": change = "0"; break;
                case "~": change = "`"; break; case "_": change = "-"; break;
                case "+": change = "="; break; case "{": change = "["; break;
                case "}": change = "]"; break; case "|": change = "\\"; break;
                case ":": change = ";"; break; case "\"": change = "'"; break;
                case "<": change = ","; break; case ">": change = "."; break;
                case "?": change = "/"; break; default: break;
            }
        }

        return w.substring(0,w.length()-1) + change;
    }

    public static HashMap<String,Double> create_ball(String w, NGramsDTE DTE) {
        // Apply all of the corrector functions to this string.
        // TreeSet<String> ball = new TreeSet<>();
        HashMap<String, Double> ball = new HashMap<>();

        // Compute the popularity of each word.
        ball.put(w, DTE.getPop(w));
        ball.put(swc_all(w), DTE.getPop(swc_all(w)));
        ball.put(swc_first(w), DTE.getPop(swc_first(w)));
        ball.put(rm_first(w), DTE.getPop(rm_first(w)));
        ball.put(rm_last(w), DTE.getPop(rm_last(w)));
        ball.put(n2s_last(w), DTE.getPop(n2s_last(w)));

        return ball;
    }

    public static void utility_test(int num_words, NGramsDTE DTE) {
        ArrayList<Double> obsv = new ArrayList<>();
        for (int i = 0; i < num_words; i++) {
            // Generate a num_words random passwords to test the utility on. These passwords will be
            // drawn from the DTE distribution.
            Random rand = new Random();
            int length = rand.nextInt(12)+10;

            String bits = "";
            for (int j = 0; j < length; j++) {
                for (int k = 0; k < 126; k++) {
                    bits += rand.nextInt(2);
                }
            }

            String word = DTE.decode(bits);
            Double out = word_utility(word.substring(1,word.length()-1), DTE);
            obsv.add(out);
        }

        Double mean = 0.0;
        for (Double x: obsv) {
            mean += x;
        }
        mean = mean/obsv.size();

        Double std = 0.0;
        for (Double x: obsv) {
            std += (x - mean)*(x - mean);
        }
        std = std/obsv.size();

        System.out.println("Mean: " + mean);
        System.out.println("Std: " + std);
    }

    public static String word_center(String w, NGramsDTE DTE) {
        // Generate the ball surrounding the word. Then create a ball for every word in that ball
        // and see if their centers are in the original ball.
        System.out.println("Word: " + w);
        HashMap<String,Double> w_ball = create_ball(w, DTE);
        System.out.println(w_ball);

        // Get the center with the highest probability.
        Double max = -1.0;
        String center = "";
        for (String x : w_ball.keySet()) {
            if (max < w_ball.get(x)) {
                max = w_ball.get(x);
                center = x;
            }
        }

        System.out.println("Center: " + center);
        return center;
    }

    public static Double word_utility(String w, NGramsDTE DTE) {
        // Generate the ball surrounding the word. Then create a ball for every word in that ball
        // and see if their centers are in the original ball.
        System.out.println("Word: " + w);
        HashMap<String,Double> w_ball = create_ball(w, DTE);
        System.out.println(w_ball);
        // Get the center with the highest probability.
        Double max = -1.0;
        String center = "";
        for (String x : w_ball.keySet()) {
            if (max < w_ball.get(x)) {
                max = w_ball.get(x);
                center = x;
            }
        }

        System.out.println(center);

        Double frac = 0.0;
        System.out.println(" ");
        w_ball.remove(w);
        if (w_ball.size() == 0) {
            return 0.0;
        }

        for (String t: w_ball.keySet()) {
            HashMap<String,Double> t_ball = create_ball(t, DTE);
            Double max2 = -1.0;
            String center2 = "";
            for (String x : t_ball.keySet()) {
                if (max2 < t_ball.get(x)) {
                    max2 = t_ball.get(x);
                    center2 = x;
                }
            }

            if (center2.equals(center)) {
                frac += 1.0;
            }
        }

        return frac/w_ball.size();
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("Hello");
        System.out.println(swc_all("HellO123"));
        System.out.println(swc_first("HellO123"));
        System.out.println(rm_first("HellO123"));
        System.out.println(rm_last("HellO123"));
        System.out.println(n2s_last("Hello12"));

        FileInputStream file = new FileInputStream("chain.ser");
        ObjectInputStream in = new ObjectInputStream(file);

        FileInputStream file2 = new FileInputStream("init.ser");
        ObjectInputStream in2 = new ObjectInputStream(file2);

        HashMap<String, LinkedHashMap<String,Double>> table =
                (HashMap<String, LinkedHashMap<String,Double>>) in.readObject();

        LinkedHashMap<String, Double> init = (LinkedHashMap<String, Double>) in2.readObject();

        NGramsDTE DTE = new NGramsDTE(table, init);

        utility_test(1000, DTE);
    }
}
