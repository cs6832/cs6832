package com.cs6832.hesafe;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.cs6832.hesafe.DTE.DTE;
import com.cs6832.hesafe.DTE.NGramsDTE;
import com.cs6832.hesafe.hesafe.R;
import com.cs6832.hesafe.vaults.Vault;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class VaultDisplay extends AppCompatActivity {
    private Vault vault;
    public NGramsDTE NGrams;
    private ArrayList<String> passList;
    private ArrayAdapter<String> adap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vault_display);

        // Read the serialized files and initialize the DTE.
        HashMap<String, LinkedHashMap<String,Double>> table = null;
        LinkedHashMap<String, Double> init = null;
        try {

            InputStream fis = getResources().openRawResource(R.raw.chain);
            ObjectInputStream in = new ObjectInputStream(fis);

            InputStream fis2 = getResources().openRawResource(R.raw.init);
            ObjectInputStream in2 = new ObjectInputStream(fis2);

            table = (HashMap<String, LinkedHashMap<String,Double>>) in.readObject();

            init = (LinkedHashMap<String, Double>) in2.readObject();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (table == null || init == null) {
            Toast.makeText(this, "The vault failed to load due to missing resource files.",
                    Toast.LENGTH_SHORT).show();

        }

        NGrams = new NGramsDTE(table, init);

        // Retrieve the user information and create the vault.
        Intent intent = getIntent();
        String vault_name = intent.getStringExtra(LoginActivity.VAULT_MESSAGE);
        final String pass_name = intent.getStringExtra((LoginActivity.PASS_MESSAGE));

        vault = new Vault(vault_name, pass_name, NGrams, this);

        // Have the vault decrypt the passwords and picture to return to the user.
        passList = vault.getPasswords();
        ListView list = (ListView) findViewById(R.id.passList);

        adap = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,passList);
        list.setAdapter(adap);

        // Have the user's picture displayed to them.
        int pic = vault.getPicture();
        System.out.println(pic);
        TypedArray images = getResources().obtainTypedArray(R.array.pic_ids);

        ImageView view = (ImageView) findViewById(R.id.imageView);
        view.setImageResource(images.getResourceId(pic, -1));


        // The add button function.
        Button add = (Button) findViewById(R.id.add_password_button);
        final EditText editText = (EditText) findViewById(R.id.edit_new_password);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = editText.getText().toString();
                passList.add(value);

                vault.addPassword(value);

                adap.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}
