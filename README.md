# Honey Safe: A practical application of Honey Encryption #

A small server-less password manager for the Android-OS that uses honey encryption.

### How to use ###

* On startup screen, select register to register a new vaultname and password
* On registration, select a password, vault name.  Additionally, select one of the 256 icons
* You are now ready to store passwords